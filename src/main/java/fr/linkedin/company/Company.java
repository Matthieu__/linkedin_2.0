package fr.linkedin.company;

import fr.linkedin.project.Project;
import fr.linkedin.user.User;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Objects;
import java.util.Set;

@Entity
@DiscriminatorValue("0")
public class Company extends User{
    @Column(name = "company_name")
    private String name;
    @Column(name = "company_description")
    private String description;
    @Column(name = "company_ville")
    private String ville;
    @Column(name = "company_nb")
    private int nbSalarie;
    @Column(name="company_image_id")
    String company_image_id ;



    @OneToMany(mappedBy="company")
    private Set<Project> projects;


    public Company() {
        super();
    }

    public Company(User user){
        super(user);
    }

    public Company(String name, String description, String ville, int nbSalarie) {
        super();
        this.name = name;
        this.description = description;
        this.ville = ville;
        this.nbSalarie = nbSalarie;
    }
    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public Company(Company company, User user) {
        super(user);
        this.name = company.getName();
        this.description = company.getDescription();
        this.ville = company.getVille();
        this.nbSalarie = company.getNbSalarie();
    }

    public void setAll(Company company){
        this.name = company.getName();
        this.description = company.getDescription();
        this.ville = company.getVille();
        this.nbSalarie = company.getNbSalarie();
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getNbSalarie() {
        return nbSalarie;
    }

    public void setNbSalarie(int nbSalarie) {
        this.nbSalarie = nbSalarie;
    }


    public int getNbsalarie() {
        return nbSalarie;
    }

    public void setNbsalarie(int nbsalarie) {
        this.nbSalarie = nbsalarie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompany_image_id() {
        return company_image_id;
    }

    public void setCompany_image_id(String company_image_id) {
        this.company_image_id = company_image_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return nbSalarie == company.nbSalarie &&
                Objects.equals(name, company.name) &&
                Objects.equals(description, company.description) &&
                Objects.equals(ville, company.ville);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, ville, nbSalarie);
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", ville='" + ville + '\'' +
                ", nbSalarie=" + nbSalarie +
                '}';
    }

}
