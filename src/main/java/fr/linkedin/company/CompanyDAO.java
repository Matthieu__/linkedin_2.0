package fr.linkedin.company;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 */

@Repository
public interface CompanyDAO extends CrudRepository<Company, Long> {

}
