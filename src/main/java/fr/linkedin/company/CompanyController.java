package fr.linkedin.company;

import fr.linkedin.project.Project;
import fr.linkedin.project.ProjectDAO;
import fr.linkedin.security.CheckRightAccessUtils;
import fr.linkedin.student.Student;
import fr.linkedin.user.UserDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RequestMapping("/company")
@Controller
public class CompanyController {

    private final CompanyDAO companyDAO;
    private final UserDAO userDAO;
    private final ProjectDAO projectDAO;
    private final CheckRightAccessUtils checkRightAccess;

    public CompanyController(CompanyDAO companyDAO, UserDAO userDAO, ProjectDAO projectDAO, CheckRightAccessUtils checkRightAccess) {
        this.companyDAO = companyDAO;
        this.userDAO = userDAO;
        this.projectDAO = projectDAO;
        this.checkRightAccess = checkRightAccess;
    }

    @PostMapping("/new")
    public String createNewCompany(@ModelAttribute Company company, HttpSession session, Model m) {
        if (!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))) {
            return "redirect:/signIn";
        }
        Company companyBDD = (Company) userDAO.findById((Long) session.getAttribute("id")).get();
        companyBDD.setAll(company);
        m.addAttribute("name", "company");
        userDAO.save(companyBDD);
        return "redirect:/user/profile";
    }

    @GetMapping
    public String companyListPage(Model m, HttpSession session) {
        if (!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))) {
            return "redirect:/signIn";
        }
        m.addAttribute("companies", companyDAO.findAll());
        return "companies";
    }

    @GetMapping("/details/{companyID}")
    public String detailsPage(Model m, @PathVariable(value = "companyID") Long id, HttpSession session) {
        if (!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))) {
            return "redirect:/signIn";
        }
        Company company = companyDAO.findById(id).get();
        List<Project> projects = projectDAO.findByCompany(company);
        m.addAttribute("company", company);
        m.addAttribute("projects", projects);
        return "company_details";
    }

    @GetMapping("/candidates/{projectID}")
    public String detailscandidates(Model m, @PathVariable(value = "projectID") Long id, HttpSession session) {
        if (!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))) {
            return "redirect:/signIn";
        }
        Project projectFindbyID = projectDAO.findById(id).get();
        Iterable<Student> students = projectFindbyID.getStudents();
        m.addAttribute("students", students);
        return "candidates";
    }
}
