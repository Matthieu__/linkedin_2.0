package fr.linkedin.project;


import fr.linkedin.company.Company;
import fr.linkedin.security.CheckRightAccessUtils;
import fr.linkedin.skill.SkillDAO;
import fr.linkedin.student.Student;
import fr.linkedin.user.UserDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;

/**
 *
 */
@RequestMapping("/project")
@Controller
public class Project_controller {

  private final ProjectDAO projectDAO;
  private final SkillDAO skillDAO ;
  private final UserDAO userDAO;
  private final CheckRightAccessUtils checkRightAccess;

  public Project_controller(ProjectDAO projectDAO, SkillDAO skillDAO, UserDAO userDAO, CheckRightAccessUtils checkRightAccess) {
    this.projectDAO = projectDAO;
    this.skillDAO = skillDAO;
      this.userDAO = userDAO;
      this.checkRightAccess = checkRightAccess;
  }

    @GetMapping
    public String homePage(Model m, HttpSession session) {
        if(!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))){
            return "redirect:/signIn";
        }
        m.addAttribute("projects", projectDAO.findAll());
        return "projects";
    }

    @GetMapping("/details/{projectID}")
    public String detailsPage(Model m, @PathVariable(value = "projectID") Long id, HttpSession session) {
        if(!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))){
            return "redirect:/signIn";
        }
    Project projectFindbyID =projectDAO.findById(id).get();

        m.addAttribute("project", projectFindbyID);
        return "project_details";
    }

    @GetMapping("/new/{companyID}")
    public String addUserPage(Model m, @PathVariable(value = "companyID") Long id, HttpSession session) {
        if(!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))){
            return "redirect:/signIn";
        }
        m.addAttribute("id", id);
        m.addAttribute("project", new Project());
        m.addAttribute("skillsSet", skillDAO.findAll());
        return "newProject";
    }

    @PostMapping("/new/{companyID}")
    public RedirectView createNewProject(@ModelAttribute Project project,
                                         @PathVariable(value = "companyID") Long id, HttpSession session) {
        if(!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))){
            return new RedirectView("/signIn");
        }
        Company company = (Company) userDAO.findById(id).get();
        project.setCompany(company);
        projectDAO.save(project);
        return new RedirectView("/user/profile");
    }

    @GetMapping("/apply/{projectID}/{userID}")
    public RedirectView ApplyProject(Model m, @PathVariable(value = "projectID") Long projectID,@PathVariable(value = "userID") Long userID, HttpSession session) {
        if(!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))){
            return new RedirectView("signIn");
        }
        Project project= projectDAO.findById(projectID).get();
        Student student= (Student)userDAO.findById(userID).get();
        student.addProject(project);
        userDAO.save(student);
        m.addAttribute("projects", projectDAO.findAll());
        return new RedirectView("/user/profile/");
    }

}
