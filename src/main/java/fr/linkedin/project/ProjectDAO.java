
package fr.linkedin.project;

import fr.linkedin.company.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectDAO extends CrudRepository<Project, Long> {
    List<Project> findByCompany(Company company);
}
