package fr.linkedin.project;

import fr.linkedin.company.Company;
import fr.linkedin.skill.Skill;
import fr.linkedin.student.Student;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity(name = "projects")
public class Project {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private  Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;


    @Column(name = "salary")
    private Integer salary;

    @Column(name = "start_date")
    private String start_date;

    @Column(name = "end_date")
    private String end_date;

    @ManyToMany @JoinTable(
            name = "skill_project",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id"))
    private Set<Skill> skills;

    @ManyToMany (mappedBy = "projects")
    private Set<Student> students;

    @ManyToOne
    @JoinColumn(name="company_id", nullable=false)
    private Company company;


    public Project(){
        super();
    }

    public Project(String name, String description, Company company, Integer salary, String start_date, String end_date, Set<Skill> skills) {
        this.name = name;
        this.description = description;
        this.company = company;
        this.salary = salary;
        this.start_date = start_date;
        this.end_date = end_date;
        this.skills = skills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(name, project.name) &&
                Objects.equals(description, project.description) &&
                Objects.equals(company, project.company) &&
                Objects.equals(salary, project.salary) &&
                Objects.equals(start_date, project.start_date) &&
                Objects.equals(end_date, project.end_date) &&
                Objects.equals(skills, project.skills);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, company, salary, start_date, end_date, skills);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

}
