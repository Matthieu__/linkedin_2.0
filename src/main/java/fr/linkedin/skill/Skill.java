package fr.linkedin.skill;


import fr.linkedin.project.Project;
import fr.linkedin.student.Student;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "skills")
public class Skill {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Id private Long id;
      @Column(name = "name") private String name;
     @ManyToMany (mappedBy = "skills")private Set<Student> students;
     @ManyToMany (mappedBy = "skills")private Set<Project> projects;
    public Skill(){
        super();
    }
    public Skill(String name, Set<Student> students, Set<Project> projects) {
        this.name = name;
        this.students = students;
        this.projects=projects;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
