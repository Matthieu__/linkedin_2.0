package fr.linkedin.skill;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SkillDAO extends CrudRepository<Skill, Long> {

}