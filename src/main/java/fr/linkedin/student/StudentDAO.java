package fr.linkedin.student;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface StudentDAO extends CrudRepository<Student, Long> {


    Optional<Student> findByFirstName(String string);
}

