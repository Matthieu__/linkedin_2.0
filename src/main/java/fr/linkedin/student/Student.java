package fr.linkedin.student;

import fr.linkedin.skill.Skill;
import fr.linkedin.user.User;
import fr.linkedin.project.Project;

import javax.persistence.*;
import java.util.Set;

@Entity
@DiscriminatorValue("1")
public class Student extends User {

    @Column(name = "student_first_name")
    String firstName;
    @Column(name = "student_last_name")
    String lastName ;
    @Column(name = "student_birth_date")
    String birth_date;
    @Column(name = "student_description")
    String description ;
    @Column(name="student_image_id")
    String student_image_id ;
    @Column(name = "student_school")
    String school;
    @Column(name = "student_city")
    String city;
    @Column(name = "student_email")
    String email;
    @Column(name = "student_linkedin")
    String linkedin;
    @ManyToMany
    @JoinTable(name = "skill_student", joinColumns = @JoinColumn(name = "user.id")   , inverseJoinColumns = @JoinColumn(name = "skill_id"))
    Set<Skill> skills;

    @ManyToMany
    @JoinTable(name = "student_project", joinColumns = @JoinColumn(name = "user.id")   , inverseJoinColumns = @JoinColumn(name = "project_rid"))
    Set<Project> projects;


    public Student() {
        super();
    }

    public Student(User user){
        super(user);
    }

    public Student(String firstName, String lastName, String age, String description, Set<Skill> skills,String school,Set<Project> projects,String city,String email,String linkedin) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birth_date = age;
        this.description = description;
        this.skills = skills;
        this.school=school;
        this.projects=projects;
        this.city=city;
        this.email= email;
        this.linkedin=linkedin;
    }

    public void setAll(Student student){
        this.firstName = student.firstName;
        this.lastName = student.lastName;
        this.birth_date = student.birth_date;
        this.description = student.description;
        this.skills = student.skills;
        this.school=student.school;
        this.city=student.city;
        this.email= student.email;
        this.linkedin= student.linkedin;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public String getDescription() {
        return description;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public String getStudent_image_id() {
        return student_image_id;
    }

    public void setStudent_image_id(String student_image_id) {
        this.student_image_id = student_image_id;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public void addProject(Project project){
        this.projects.add(project);
    }
}
