package fr.linkedin.student;


import fr.linkedin.project.Project;
import fr.linkedin.security.CheckRightAccessUtils;
import fr.linkedin.user.UserDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;

/**
 *
 */
@RequestMapping("/student")
@Controller
public class StudentController {

    private final StudentDAO studentDAO;
    private final UserDAO userDAO;
    private final CheckRightAccessUtils checkRightAccess;

    public StudentController(StudentDAO userDAO, UserDAO userDAO1, CheckRightAccessUtils checkRightAccess) {
        this.studentDAO = userDAO;
        this.userDAO = userDAO1;
        this.checkRightAccess = checkRightAccess;
    }

    @GetMapping
    public String homePage(Model m, HttpSession session) {
      if (!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))) {
        return "redirect:/signIn";
      }
        m.addAttribute("students", studentDAO.findAll());
        return "students";
    }

    @PostMapping("/new")
    public RedirectView createNewStudent(Model m,
                                         @ModelAttribute Student student, HttpSession session) {
      if (!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))) {
        return new RedirectView("/signIn");
      }
        Student studentBDD = (Student) userDAO.findById((Long) session.getAttribute("id")).get();
        studentBDD.setAll(student);
        m.addAttribute("name", "student");
        userDAO.save(studentBDD);
        return new RedirectView("/user/profile");
    }

    @GetMapping("/details/{studentID}")
    public String detailsPage(Model m, @PathVariable(value = "studentID") Long id, HttpSession session) {
      if (!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))) {
        return "redirect:/signIn";
      }
        Student studentFindbyID = studentDAO.findById(id).get();

        m.addAttribute("student", studentFindbyID);
        return "student_details";
    }

    @GetMapping("/demande/{studentID}")
    public String detailsapply(Model m, @PathVariable(value = "studentID") Long id, HttpSession session) {
      if (!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))) {
        return "redirect:/signIn";
      }
        Student studentFindbyID = studentDAO.findById(id).get();
        Iterable<Project> projects = studentFindbyID.getProjects();
        m.addAttribute("projects", projects);
        return "student_apply";
    }
}
