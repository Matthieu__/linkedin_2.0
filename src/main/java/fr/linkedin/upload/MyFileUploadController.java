package fr.linkedin.upload;


import fr.linkedin.company.Company;
import fr.linkedin.student.Student;
import fr.linkedin.user.UserDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Files;

@RequestMapping("/photo")
@Controller
public class MyFileUploadController {
    private StorageService storageService;

    public MyFileUploadController(StorageService storageService, UserDAO userDAO) {
        this.storageService = storageService;
        this.userDAO = userDAO;
    }
    private String name;
    private final UserDAO userDAO;

    // POST: Do Upload

    @PostMapping( "/upload-file/company/{id}")
    public  RedirectView uploadOneFileCompanyHandlerPOST(@PathVariable(value = "id")Long id,
                                                         @RequestParam("file") MultipartFile file, Model model, HttpSession session) {
            name = storageService.store(file);
            Company company = (Company) userDAO.findById(id).get();
            company.setCompany_image_id(name);
            userDAO.save(company);
        model.addAttribute("files",name);
        return new RedirectView("/user/profile");
    }

    @PostMapping( "/upload-file/student/{id}")
    public  RedirectView uploadOneFileStudentHandlerPOST(@PathVariable(value = "id")Long id,
                                                         @RequestParam("file") MultipartFile file, Model model, HttpSession session) {
        name = storageService.store(file);
        Student company = (Student) userDAO.findById(id).get();
        company.setStudent_image_id(name);
        userDAO.save(company);
        model.addAttribute("files",name);
        return new RedirectView("/user/profile");
    }

    @RequestMapping("/image/{name}")
    @ResponseBody
    public byte[] getImage(@PathVariable(value = "name")String name)throws IOException{
        return Files.readAllBytes(storageService.load(name));
    }

    @RequestMapping("/image/dbInit/{name}")
    @ResponseBody
    public byte[] getImageForInit(@PathVariable(value = "name")String name)throws IOException{
        return Files.readAllBytes(storageService.load("dbInit/" + name));
    }

}