package fr.linkedin.user;

import fr.linkedin.security.PasswordUtils;
import fr.linkedin.security.UserRoles;

import javax.persistence.*;
import java.util.UUID;

@Entity(name="Users")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="user_role",
        discriminatorType = DiscriminatorType.INTEGER)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "role")
    private UserRoles role;
    @Column(name = "token")
    private String token;



    public User(){ }

    public User(User user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.role = user.getRole();
        this.token = user.getToken();
    }

    public User(String username, String password, UserRoles role) {
        PasswordUtils passwordUtils = new PasswordUtils();
        this.username = username;
        this.password = passwordUtils.hashPassword(password);
        this.role = role;
        this.token = UUID.randomUUID().toString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRoles getRole() {
        return role;
    }

    public void setRole(UserRoles role) {
        this.role = role;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
