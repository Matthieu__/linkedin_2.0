package fr.linkedin.user;

import fr.linkedin.company.Company;
import fr.linkedin.company.CompanyDAO;
import fr.linkedin.project.Project;
import fr.linkedin.project.ProjectDAO;
import fr.linkedin.security.CheckRightAccessUtils;
import fr.linkedin.security.UserRoles;
import fr.linkedin.skill.Skill;
import fr.linkedin.student.Student;
import fr.linkedin.student.StudentDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RequestMapping("/user")
@Controller
public class UserController {

    private final UserDAO userDAO;
    private final CheckRightAccessUtils checkRightAccess;
    private final CompanyDAO companyDAO;
    private final StudentDAO studentDAO;
    private final ProjectDAO projectDAO;

    public UserController(UserDAO userDAO, CheckRightAccessUtils checkRightAccess, CompanyDAO companyDAO, StudentDAO studentDAO, ProjectDAO projectDAO) {
        this.userDAO = userDAO;
        this.checkRightAccess = checkRightAccess;
        this.companyDAO = companyDAO;
        this.studentDAO = studentDAO;
        this.projectDAO = projectDAO;
    }

    @GetMapping("/profile")
    public String signInPage(HttpSession session, Model m) {
        if (!checkRightAccess.isLoggedIn((String) session.getAttribute("token"))) {
            return "redirect:/signIn";
        }
        String token = (String) session.getAttribute("token");
        User user = userDAO.findByToken(token);
        Long id = user.getId();
        if (UserRoles.COMPANY.equals(user.getRole())) {
            Company company = companyDAO.findById(id).get();
            List<Project> projects = projectDAO.findByCompany(company);
            m.addAttribute("company", company);
            m.addAttribute("projects", projects);
            return "company_profile";
        } else {
            Student studentFindbyID = studentDAO.findById(id).get();
            Iterable<Project> projects = projectDAO.findAll();

            Set<Project> projectsInSet = new HashSet<>();
            for (Skill skillStudent : studentFindbyID.getSkills()) {
                for (Project project : projects) {
                    for (Skill skillProject : project.getSkills()) {
                        if (skillStudent.getName().equals(skillProject.getName())) {
                            projectsInSet.add(project);
                        }
                    }
                }
            }

            m.addAttribute("projectsskills", projectsInSet);
            m.addAttribute("projects", projects);
            m.addAttribute("student", studentFindbyID);

            return "student_profile";
        }
    }
}
