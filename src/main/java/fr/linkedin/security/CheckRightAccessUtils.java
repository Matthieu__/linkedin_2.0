package fr.linkedin.security;


import fr.linkedin.user.UserDAO;
import org.springframework.stereotype.Service;


@Service
public class CheckRightAccessUtils {

    private final UserDAO userDAO;

    public CheckRightAccessUtils(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public boolean isLoggedIn(String token){
        if(token == null || token.isEmpty()) return false;
        return userDAO.findByToken(token) != null;
    }
}
