package fr.linkedin.security;

import fr.linkedin.company.Company;
import fr.linkedin.skill.SkillDAO;
import fr.linkedin.student.Student;
import fr.linkedin.user.User;
import fr.linkedin.user.UserDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;

@RequestMapping()
@Controller
public class LoginController {

    private final UserDAO userDAO;
    private final PasswordUtils passwordUtils;
    private final SkillDAO skillDAO;

    public LoginController(UserDAO userDAO, PasswordUtils passwordUtils, SkillDAO skillDAO) {
        this.userDAO = userDAO;
        this.passwordUtils = passwordUtils;
        this.skillDAO = skillDAO;
    }

    @RequestMapping("*")
    public String defaultRedirect() {
        return "redirect:/signIn";
    }

    @GetMapping("/signIn")
    public String signInPage() {
        return "signIn";
    }

    @GetMapping("/logOut")
    public RedirectView lougOut(HttpSession session) {
        session.removeAttribute("token");
        return new RedirectView("/signIn");
    }

    @PostMapping("/signIn")
    public RedirectView login(RedirectAttributes redirectAttributes, HttpSession session,
                              @RequestParam("username") String username,
                              @RequestParam("password") String password) {
        User user = userDAO.findByUsername(username);
        if (user == null) {
            redirectAttributes.addFlashAttribute("error", "Wrong username");
            return new RedirectView("/signIn");
        }

        if (!passwordUtils.checkPassword(password, user.getPassword())) {
            redirectAttributes.addFlashAttribute("error", "Wrong password");
            return new RedirectView("/signIn");
        }

        session.setAttribute("token", user.getToken());
        return new RedirectView("/user/profile");
    }

    @GetMapping("/register")
    public String registerPage(Model m) {
        return "register";
    }

    @PostMapping("/register")
    public String register(Model m,RedirectAttributes redirectAttributes,
                           @RequestParam("username") String username,
                           @RequestParam("password") String password,
                           @RequestParam("passwordConfirm") String passwordConfirm,
                           @RequestParam("role") UserRoles role, HttpSession session) {
        if(userDAO.findByUsername(username) != null){
            redirectAttributes.addFlashAttribute("error", "This username is already used");
            return "redirect:/register";
        }
        if (!password.equals(passwordConfirm)) {
            redirectAttributes.addFlashAttribute("error", "Not the same password");
            return "redirect:/register";
        }

        if (role == UserRoles.COMPANY) {
            Company company = new Company(new User(username, password, role));
            User user = userDAO.save(company);
            session.setAttribute("token", user.getToken());
            session.setAttribute("id", user.getId());
            m.addAttribute("company", new Company());
            return "newCompany";
        } else {
            Student student = new Student(new User(username, password, role));
            User user = userDAO.save(student);
            session.setAttribute("token", user.getToken());
            session.setAttribute("id", user.getId());
            m.addAttribute("student", new Student());
            m.addAttribute("allSkills", skillDAO.findAll());
            return "newStudent";
        }
    }
}
