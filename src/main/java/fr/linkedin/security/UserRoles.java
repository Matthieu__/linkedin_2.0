package fr.linkedin.security;

public enum UserRoles {
    STUDENT("Student"),
    COMPANY("Company");

    private String displayName = "";

    UserRoles(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
