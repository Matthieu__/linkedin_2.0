package fr.linkedin.security;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class PasswordUtils {

    public String hashPassword(String password){
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public Boolean checkPassword(String password, String hash){
        return BCrypt.checkpw(password, hash);
    }
}
