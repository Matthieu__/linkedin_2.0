create table users
(
    id                  bigint auto_increment
        primary key,
    username            text        null,
    password            text        null,
    role                varchar(30) null,
    user_role           int         null,
    token               text        null,
    company_image_id    text        null,
    company_name        text        null,
    company_description text        null,
    company_nb          int         null,
    company_ville       text        null,
    student_first_name  text        null,
    student_last_name   text        null,
    student_birth_date  text        null,
    student_image_id    text        null,
    student_description text        null,
    student_school      text        null,
    student_city        text        null,
    student_email       text        null,
    student_linkedin    text        null
);

create table projects
(
    id          bigint auto_increment
        primary key,
    name        text null,
    description text null,
    company_id      bigint  NULL,
    salary      int  null,
    start_date  text null,
    end_date    text null,
    constraint company_id
        foreign key (company_id) references users (id)
);

create table skills
(
    id bigint auto_increment,
    name text null,
    primary key (id)
);

create table skill_project
(
    skill_id bigint  NULL,
    project_id bigint  NULL,
    constraint skill_id
        foreign key (skill_id) references skills (id),
    constraint project_id
        foreign key (project_id) references projects(id)
);

create table skill_student
(
    skill_id bigint  NULL,
    user_id bigint  NULL,
    CONSTRAINT `skill_uid`
        FOREIGN KEY (skill_id) REFERENCES skills (id),
    CONSTRAINT `user_sid`
        FOREIGN KEY (user_id) REFERENCES users (id)
);

create table student_project
(
    user_id bigint  NULL,
    project_rid bigint  NULL,
    CONSTRAINT `skill_tid`
        FOREIGN KEY (user_id) REFERENCES users (id),
    CONSTRAINT `user_rid`
        FOREIGN KEY (project_rid) REFERENCES projects (id)
);




/*
create table students
(
    first_name  text null,
    last_name   text null,
    birth_date  int  null,
    description text null,
    image_id    text null,
    id bigint auto_increment,
    primary key (id)
);
create table skills
(
    id bigint auto_increment,
    name text null,
    primary key (id)
);


create table skill_student
(
    id bigint auto_increment
        primary key,
    skill_id bigint  NULL,
    student_id bigint NULL,
    CONSTRAINT `skill_id`
        FOREIGN KEY (skill_id) REFERENCES skills (id),
    CONSTRAINT `student_id`
        FOREIGN KEY (student_id) REFERENCES students (id)

);
create table skill_student
(
    skill_id bigint  NULL,
    student_id bigint  NULL,
    CONSTRAINT `skill_id`
        FOREIGN KEY (skill_id) REFERENCES skills (id),
    CONSTRAINT `student_id`
        FOREIGN KEY (student_id) REFERENCES students (id)

);

create table projects
(
    id          bigint auto_increment
        primary key,
    name        text null,
    description text null,
    company     text null,
    salary      int  null,
    start_date  text null,
    end_date    text null,
    imageID     text null
);

create table skill_project
(
    id bigint auto_increment
        primary key,
    skill2_id bigint  NULL,
    project_id bigint NULL,
    constraint skill2_id
        foreign key (skill2_id) references skills (id),
    constraint project_id
        foreign key (project_id) references projects(id)
);

create table skill_project
(
    skill2_id bigint  NULL,
    project_id bigint  NULL,
    constraint skill2_id
        foreign key (skill2_id) references skills (id),
    constraint project_id
        foreign key (project_id) references projects(id)
);


*/
