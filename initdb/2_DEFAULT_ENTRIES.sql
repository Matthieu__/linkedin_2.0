INSERT INTO defaultdb.skills (id, name)
VALUES (1, 'Java');
INSERT INTO defaultdb.skills (id, name)
VALUES (2, 'C');
INSERT INTO defaultdb.skills (id, name)
VALUES (3, 'Git');
INSERT INTO defaultdb.skills (id, name)
VALUES (4, 'Spring');
INSERT INTO defaultdb.skills (id, name)
VALUES (5, 'Bootstrap');
INSERT INTO defaultdb.skills (id, name)
VALUES (6, 'Angular');


INSERT INTO defaultdb.users (id, role, user_role, company_image_id, company_name, company_description, company_nb,
                             company_ville,username,password,token)
VALUES (2, 1, 0, 'dbInit/epf.png', 'EPF',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed felis iaculis, dictum arcu eget, lacinia elit. Phasellus id semper nisi. Nulla sem ante, congue at magna egestas, aliquet pulvinar sem. Mauris molestie orci elit, ut gravida sem blandit vel.',
        500, 'Paris','epf','$2a$10$ka/yCe4wa7S0syMYo8w.0OexqfoVaISLg/I6sXO7wAK8P7APHrkS.','f1d09994-f2c4-4ecd-ab2a-0ab6b01fce34');

INSERT INTO defaultdb.users (id, role, user_role, company_image_id, company_name, company_description, company_nb,
                             company_ville,username,password,token)
VALUES (1, 1, 0, 'dbInit/microsoft.png', 'Microsoft', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed felis iaculis, dictum arcu eget, lacinia elit. Phasellus id semper nisi. Nulla sem ante, congue at magna egestas, aliquet pulvinar sem. Mauris molestie orci elit, ut gravida sem blandit vel.',
        10000, 'Los Angeles','microsoft','$2a$10$ka/yCe4wa7S0syMYo8w.0OexqfoVaISLg/I6sXO7wAK8P7APHrkS.','f46471fb-c3cd-4955-b569-f85e9c33f759');

INSERT INTO defaultdb.users (id,username,password,role,user_role,token, student_first_name , student_last_name  ,    student_birth_date    ,
    student_image_id ,student_description , student_school, student_city , student_email, student_linkedin )
VALUES (3,'pierre','$2a$10$ka/yCe4wa7S0syMYo8w.0OexqfoVaISLg/I6sXO7wAK8P7APHrkS.',0,1,'d0743a40-805a-4956-b529-a4fbe532d6ae','Pierre','Bechara','1997-01-01'
        ,'dbInit/pierre.png','I have not been able to discover many translations of the epigrams so I took the liberty of translating them myself. I took other liberties too. These translations differ in three major respects from the originals.'
        ,'EPF','Paris','pierre.bechara@epfedu.fr',  'https://www.linkedin.com/in/pierre-bechara-a09864158/'
        );

INSERT INTO defaultdb.users (id,username,password,role,user_role,token, student_first_name , student_last_name  ,    student_birth_date    ,
    student_image_id ,student_description , student_school, student_city , student_email, student_linkedin )
VALUES (4,'matthieu','$2a$10$ka/yCe4wa7S0syMYo8w.0OexqfoVaISLg/I6sXO7wAK8P7APHrkS.',0,1,'f9c98ca1-7e17-4b3e-a984-55400d54fb4d','Matthieu','Vivares','1997-01-01'
        ,'dbInit/matthieu.png','While others had shown that English poetry could stand on its own feet (pardon the pun) Campion sought to revive “classical numbers” or a quantitative versification where the syllables are arranged according to their length.'
        ,'EPF','Paris','matthieu.vivares@epfedu.fr',  'https://www.linkedin.com/in/matthieuvivares/'
        );

INSERT INTO defaultdb.users (id,username,password,role,user_role,token, student_first_name , student_last_name  ,    student_birth_date    ,
    student_image_id ,student_description , student_school, student_city , student_email, student_linkedin )
VALUES (5,'mathilde','$2a$10$ka/yCe4wa7S0syMYo8w.0OexqfoVaISLg/I6sXO7wAK8P7APHrkS.',0,1,'d7fe8615-9ef7-401d-b7df-4e0e2515a037','Mathilde','Lorrain','1997-01-01'
        ,'dbInit/mathilde.png','There are constant battles among poets and critics over what is proper and poetic in matters of prosody.'
        ,'EPF','Paris','mathilde.lorrain@epfedu.fr',  'https://www.linkedin.com/in/mathilde-lorrain-b58348151/'
        );


insert into defaultdb.skill_student (skill_id,user_id) values
((select id from skills where name = 'Java'), (select id from users where username = 'Pierre'));
insert into defaultdb.skill_student (skill_id,user_id) values
((select id from skills where name = 'Git'), (select id from users where username = 'Pierre'));
insert into defaultdb.skill_student (skill_id,user_id) values
((select id from skills where name = 'Spring'), (select id from users where username = 'Pierre'));


insert into defaultdb.skill_student (skill_id,user_id) values
((select id from skills where name = 'Bootstrap'), (select id from users where username = 'Matthieu'));
insert into defaultdb.skill_student (skill_id,user_id) values
((select id from skills where name = 'Angular'), (select id from users where username = 'Matthieu'));

insert into defaultdb.skill_student (skill_id,user_id) values
((select id from skills where name = 'Java'), (select id from users where username = 'Mathilde'));
insert into defaultdb.skill_student (skill_id,user_id) values
((select id from skills where name = 'C'), (select id from users where username = 'Mathilde'));
insert into defaultdb.skill_student (skill_id,user_id) values
((select id from skills where name = 'Angular'), (select id from users where username = 'Mathilde'));


INSERT INTO defaultdb.projects (name, description, company_id, salary, start_date, end_date, id)
VALUES ( 'Dev Frontend'
       , 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed felis iaculis, dictum arcu eget, lacinia elit. Phasellus id semper nisi. Nulla sem ante, congue at magna egestas, aliquet pulvinar sem. Mauris molestie orci elit, ut gravida sem blandit vel. Cras vulputate hendrerit libero tincidunt commodo. Cras gravida, nisl dapibus finibus lobortis, tortor nibh vestibulum eros, et condimentum tellus libero quis ipsum. '
       , 1
       , 1500, '05/05/2019', '05/07/2019', 1);

INSERT INTO defaultdb.projects (name, description, company_id, salary, start_date, end_date, id)
VALUES ( 'Dev Java Backend'
       , 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed felis iaculis, dictum arcu eget, lacinia elit. Phasellus id semper nisi. Nulla sem ante, congue at magna egestas, aliquet pulvinar sem. Mauris molestie orci elit, ut gravida sem blandit vel. Cras vulputate hendrerit libero tincidunt commodo. Cras gravida, nisl dapibus finibus lobortis, tortor nibh vestibulum eros, et condimentum tellus libero quis ipsum. '
       , 1
       , 1532, '05/05/2019', '05/07/2019', 2);

INSERT INTO defaultdb.projects (name, description, company_id, salary, start_date, end_date, id)
VALUES ( 'Dev mySQL'
       , 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed felis iaculis, dictum arcu eget, lacinia elit. Phasellus id semper nisi. Nulla sem ante, congue at magna egestas, aliquet pulvinar sem. Mauris molestie orci elit, ut gravida sem blandit vel. Cras vulputate hendrerit libero tincidunt commodo. Cras gravida, nisl dapibus finibus lobortis, tortor nibh vestibulum eros, et condimentum tellus libero quis ipsum. '
       , 2
       , 423, '05/05/2019', '05/07/2019', 3);


INSERT INTO defaultdb.projects (name, description, company_id, salary, start_date, end_date, id)
VALUES ( 'Dev PHP'
       , 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed felis iaculis, dictum arcu eget, lacinia elit. Phasellus id semper nisi. Nulla sem ante, congue at magna egestas, aliquet pulvinar sem. Mauris molestie orci elit, ut gravida sem blandit vel. Cras vulputate hendrerit libero tincidunt commodo. Cras gravida, nisl dapibus finibus lobortis, tortor nibh vestibulum eros, et condimentum tellus libero quis ipsum. '
       , 1
       , 423, '05/05/2019', '05/07/2019', 4);

INSERT INTO defaultdb.projects (name, description, company_id, salary, start_date, end_date, id)
VALUES ( 'Dev Ops'
       , 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed felis iaculis, dictum arcu eget, lacinia elit. Phasellus id semper nisi. Nulla sem ante, congue at magna egestas, aliquet pulvinar sem. Mauris molestie orci elit, ut gravida sem blandit vel. Cras vulputate hendrerit libero tincidunt commodo. Cras gravida, nisl dapibus finibus lobortis, tortor nibh vestibulum eros, et condimentum tellus libero quis ipsum. '
       , 1
       , 423, '05/05/2019', '05/07/2019', 5);



insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'Java'), (select id from projects where name = 'Dev Frontend'));
insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'Bootstrap'), (select id from projects where name = 'Dev Frontend'));
insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'Angular'), (select id from projects where name = 'Dev Frontend'));

insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'Git'), (select id from projects where name = 'Dev Java Backend'));
insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'C'), (select id from projects where name = 'Dev Java Backend'));

insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'Java'), (select id from projects where name = 'Dev mySQL'));
insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'Spring'), (select id from projects where name = 'Dev mySQL'));

insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'Java'), (select id from projects where name = 'Dev Ops'));
insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'Spring'), (select id from projects where name = 'Dev Ops'));


insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'Java'), (select id from projects where name = 'Dev PHP'));
insert into defaultdb.skill_project (skill_id, project_id)
values ((select id from skills where name = 'Spring'), (select id from projects where name = 'Dev PHP'));

insert into defaultdb.student_project (project_rid,user_id) values
((select id from projects where id = 1), (select id from users where username = 'Mathilde'));
insert into defaultdb.student_project (project_rid,user_id) values
((select id from projects where id = 2), (select id from users where username = 'Mathilde'));
insert into defaultdb.student_project (project_rid,user_id) values
((select id from projects where id = 3), (select id from users where username = 'Pierre'));
insert into defaultdb.student_project (project_rid,user_id) values
((select id from projects where id = 4), (select id from users where username = 'Matthieu'));